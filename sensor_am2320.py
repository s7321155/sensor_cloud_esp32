import time
from micropython import const

class Sensor_AM2320:
    # Sensor Define Value
    AM2320_I2C_ADDR                         = const(0x5C)
    AM2320_I2C_WAKEUP_CMD                   = [0x00]
    AM2320_I2C_READ_CMD                     = [0x03,0x00,0x04]
    
    def __am2320_check_crc(self,data):
        crc = 0xffff
        for index in range(len(data)):
            crc ^= data[index]
            for bit in range(8):
                result = crc & 0x0001
                if result > 0:
                    crc >>= 1
                    crc ^= 0xA001
                else:
                    crc >>= 1
                crc &= 0xffff
        if crc == 0:
            return True
        return False
    
    def __am2320_get_value(self,data):
        hum          = int.from_bytes(data[2:4],"Big",False)
        hum         /= 10.0
        temp         = int.from_bytes(data[4:6],"Big",True)
        temp        /= 10.0
        return temp,hum
    
    def __init__(self, i2c_dev):
        self.__i2c_dev = i2c_dev
    
    def read_temperature_humidity_value(self):
        #WakeUp
        try:
            self.__i2c_dev.writeto(self.AM2320_I2C_ADDR, bytes(self.AM2320_I2C_WAKEUP_CMD))
        except OSError:
            pass
            #print('AM2320 WakeUp')
        time.sleep_ms(2)
        #Set Read Register Data Command
        try:
            self.__i2c_dev.writeto(self.AM2320_I2C_ADDR, bytes(self.AM2320_I2C_READ_CMD))
        except OSError:
            print('AM2320 Write No ACK')
            return False, 0, 0

        time.sleep_ms(2)
        #Read Register Data
        try:
            data = self.__i2c_dev.readfrom(self.AM2320_I2C_ADDR, 8)
            if self.__am2320_check_crc(data) == True:
                temp,hum = self.__am2320_get_value(data)
                return True, temp, hum
        except OSError:
            print('AM2320 Read No ACK')
            return False, 0, 0