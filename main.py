from machine import Pin, I2C
import time, socket
import sensor_am2320, sensor_sgp30, network_wlan

def web_page(string_array):
    html =""" <!DOCTYPE html>
        <html lang="en">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>ESP32 Web Server</title>
        </head>
        <body>
        <p>Temperature = """+string_array[0]+""" &deg;C</p>
        <p>Humidity = """+string_array[1]+""" %</p>
        <p>CO2 = """+string_array[2]+""" ppm</p>
        <p>TVOC = """+string_array[3]+""" ppb</p>
        </body>
        </html>"""
    return html

def main():
    #Hardware Initial
    hw_i2c  = I2C(0, scl=Pin(23), sda=Pin(21), freq=100000)
    button  = Pin(0, Pin.IN)
    lamp    = Pin(2, Pin.OUT)
    lamp.value(0)
    #Sensor Initial
    sensor_th  = sensor_am2320.Sensor_AM2320(hw_i2c)
    sensor_gas = sensor_sgp30.Sensor_SGP30(hw_i2c)
    #Get Temperature and Humidity Vaule
    while True:
        res_bool, temp, hum = sensor_th.read_temperature_humidity_value()
        if res_bool == True:
            break
        time.sleep(1)
    #Get GAS Value
    while True:
        res_bool = sensor_gas.initial_gas()
        if res_bool == True:
            break
        time.sleep(1)
    while True:
        res_bool, co2, tvoc = sensor_gas.read_gas_value(hum, temp)
        if res_bool == True:
            break
        time.sleep(1)
    print('Temperature = {:4.1f} Humidity = {:4.1f} CO2 = {:d} TVOC = {:d}'.format(temp,hum,co2,tvoc))
    #WiFi Initial
    ssid          = 'WiFi_Linag'
    password      = 'Chen_7321155'
    wlan          =  network_wlan.Network_WiFi(ssid, password)
    if wlan.connect() == False:
        print('WiFi Connect Fault!')
        return
    #Web Server Initial
    web_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    web_socket.bind(('', 80))
    web_socket.listen(5)
    # Set LED Lamp ON
    led_value = 1
    lamp.value(led_value)
    while True:
        conn, addr = web_socket.accept()
        print('Got a connection from %s' % str(addr))
        request = conn.recv(1024)
        print('Content = %s' % str(request))
        #Get Web Update Value
        res_bool, res_value1, res_value2 = sensor_th.read_temperature_humidity_value()
        if res_bool == True:
            temp = res_value1
            hum  = res_value2
        res_bool, res_value1, res_value2 = sensor_gas.read_gas_value()
        if res_bool == True:
            co2  = res_value1
            tvoc = res_value2        
        string  = '{:4.1f}/{:4.1f}/{:d}/{:d}'.format(temp,hum,co2,tvoc)
        result  = string.split('/')
        print(result)
        #Response HTML Message
        response = web_page(result)
        conn.send('HTTP/1.1 200 OK\n')
        conn.send('Content-Type: text/html\n')
        conn.send('Connection: close\n\n')
        conn.sendall(response)
        conn.close()
        #LED Lamp Change
        led_value ^= 1
        lamp.value(led_value)
        
if __name__ == '__main__':
    main()