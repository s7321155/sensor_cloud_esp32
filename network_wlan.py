import time, network

class Network_WiFi:
    def __init__(self, essid, password):
        self.__essid    = essid
        self.__password = password
        self.__wlan     = network.WLAN(network.STA_IF)
    
    def connect(self):
        try:
            self.__wlan.active(False)
            self.__wlan.active(True)
            self.__wlan.connect(self.__essid,self.__password)
            print('Start to connect WiFi')
            tick_start = time.ticks_ms()
            tick_sec   = time.ticks_ms()
            while time.ticks_diff(time.ticks_ms(), tick_start) < 60000:
                if self.__wlan.isconnected():
                    break
                if time.ticks_diff(time.ticks_ms(), tick_sec) >= 1000:
                    sec      = time.ticks_diff(time.ticks_ms(), tick_start) / 1000
                    tick_sec = time.ticks_ms()
                    print('Try to connect WiFi in {:<2.0f}s'.format(sec))
            res_connect = self.__wlan.isconnected()
            if res_connect == True:
                print('WiFi connection OK!')
                print('Network Config=',self.__wlan.ifconfig())                
            else:
                print('WiFi connection Error')
            return res_connect         
        except Exception as e: 
            print(e)
            return False