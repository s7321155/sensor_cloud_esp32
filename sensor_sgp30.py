import time, math
from micropython import const

class Sensor_SGP30:
    # Sensor Define Value
    SGP30_I2C_ADDR                          = const(0x58)
    SGP30_I2C_GET_SERIAL_ID_CMD             =[0x36,0x82]
    SGP30_I2C_INIT_AIR_QUALITY_CMD          =[0x20,0x03]
    SGP30_I2C_MEASURE_AIR_QUALITY_CMD       =[0x20,0x08]
    SGP30_I2C_SET_HUMIDITY_CMD              =[0x20,0x61]
    
    def __sgp30_crc(self,check_enable,data):
        crc = 0xff
        for index in range(len(data)):
            crc ^= data[index]
            for bit in range(8):
                result = crc & 0x80
                if result > 0:
                    crc <<= 1
                    crc ^= 0x31
                else:
                    crc <<= 1
            crc &= 0xff
        res_bool = True
        if check_enable == True and crc != 0:
            res_bool = False
        return res_bool, crc
    
    def __sgp30_get_humidity_compensation_value(self,humidity,temperature):
        e_sat           = 6.11 * math.pow(10.0, (7.5 * temperature / (237.7 + temperature)))
        vapor_pressure  = (humidity * e_sat) / 100
        abs_humidity    = 1000 * vapor_pressure * 100 / ((temperature + 273) * 461.5)
        print('absHumidity={:6.2f} g/m^3'.format(abs_humidity))
        number          = math.floor(abs_humidity * 256.0)
        return number
    
    def __sgp30_get_humidity_compensation_value(self,humidity,temperature):
        e_sat           = 6.11 * math.pow(10.0, (7.5 * temperature / (237.7 + temperature)))
        vapor_pressure  = (humidity * e_sat) / 100
        abs_humidity    = 1000 * vapor_pressure * 100 / ((temperature + 273) * 461.5)
        print('absHumidity={:6.2f} g/m^3'.format(abs_humidity))
        number          = math.floor(abs_humidity * 256.0)
        return number
    
    def __is_sgp30_detected(self):
        #Get Serial ID Command
        try:
            self.__i2c_dev.writeto(self.SGP30_I2C_ADDR, bytes(self.SGP30_I2C_GET_SERIAL_ID_CMD))
        except OSError:
            print('SGP30 Get Serial ID CMD No ACK')
            return False        
        time.sleep_ms(2)
        #Get Serial ID Data
        try:
            data        = self.__i2c_dev.readfrom(self.SGP30_I2C_ADDR, 9)
            res_bool    = True
            id_array    = bytearray(6)
            id_cnt      = 0
            for index in range(0,9,3):
                res  = self.__sgp30_crc(True, data[index:index+3])
                if res[0] == False:
                    res_bool = False                   
                    break
                else:
                    id_array[id_cnt:id_cnt+2] = data[index:index+2]
                    id_cnt += 2
            print('Serial ID={}'.format(id_array))
            return res_bool            
        except OSError:
            print('SGP30 Read No ACK')
            return False
    
    def __sgp30_init_air_quality(self):
        res_bool = False
        if self.__is_sgp30_detected() == True:
            time.sleep_ms(2)
            #Set Init Air Quality Command
            try:
                self.__i2c_dev.writeto(self.SGP30_I2C_ADDR, bytes(self.SGP30_I2C_INIT_AIR_QUALITY_CMD))
                res_bool = True
            except OSError:
                print('SGP30 Init Air Quality CMD No ACK')
        return res_bool
        
    def __sgp30_set_humidity(self,compensation):
        data      = bytearray(5)
        data[0:2] = bytes(self.SGP30_I2C_SET_HUMIDITY_CMD)
        data[2:4] = compensation.to_bytes(2, 'big')
        res       = self.__sgp30_crc(False, data[2:4])
        data[4]   = res[1]
        try:
            self.__i2c_dev.writeto(self.SGP30_I2C_ADDR, data)
            return True
        except OSError:
            print('SGP30 Set Humidity Compensation CMD No ACK')
            return 
    
    def __init__(self, i2c_dev):
        self.__i2c_dev     = i2c_dev

    def initial_gas(self):
        res_bool = self.__sgp30_init_air_quality();
        time.sleep_ms(12)
        return res_bool

    def read_gas_value(self,humidity = None, temperature = None):
        #Set Humidity Compensation Command
        if humidity != None and temperature != None:
            set_value = self.__sgp30_get_humidity_compensation_value(humidity,temperature)
            if self.__sgp30_set_humidity(set_value) == False:
                return False, 0, 0
            time.sleep_ms(1000)
        #Set Measure Air Quality Command
        try:
            self.__i2c_dev.writeto(self.SGP30_I2C_ADDR, bytes(self.SGP30_I2C_MEASURE_AIR_QUALITY_CMD))
        except OSError:
            print('SGP30 Measure Air Quality CMD No ACK')
            return False, 0, 0           
        time.sleep_ms(12)
        #Get Measure Air Quality Data
        try:
            data     = self.__i2c_dev.readfrom(self.SGP30_I2C_ADDR, 6)
            res_bool = True
            for index in range(0,6,3):
                res  = self.__sgp30_crc(True, data[index:index+3])
                if res[0] == False:
                    res_bool = False                   
                    break
            co2  = int.from_bytes(data[0:2],"Big",False)
            tvoc = int.from_bytes(data[3:5],"Big",False)
            return res_bool, co2, tvoc
        except OSError:
            print('SGP30 Read No ACK')
            return False, 0, 0 